# Maintainer: Max Harmathy <max.harmathy@web.de>
# Contributor: Jan de Groot <jgc@archlinux.org>
# Contributor: Michal Krenek <mikos@sg1.cz>

pkgname=ttf-dejavu-emojiless
pkgver=2.37
pkgrel=2
pkgdesc="Derivate of DejaVu without characters listed as emoji, in order not to override color fonts"
arch=('any')
url="https://dejavu-fonts.github.io/"
license=('custom')
depends=('fontconfig' 'xorg-fonts-encodings' 'xorg-mkfontscale' 'xorg-mkfontdir')
makedepends=('fontforge')
provides=('ttf-font' 'ttf-dejavu')
conflicts=('ttf-dejavu')
source=(http://downloads.sourceforge.net/project/dejavu/dejavu/${pkgver}/dejavu-fonts-ttf-${pkgver}.tar.bz2
        https://github.com/iamcal/emoji-data/raw/master/emoji.json
        cleaner.py
        remove-generic-name-assignment-and-aliasing.patch)
sha256sums=('fa9ca4d13871dd122f61258a80d01751d603b4d3ee14095d65453b4e846e17d7'
            '21edd84405e44967b2590b81cee98a84de5807e54e660b05559ed8bafc38ebd6'
            '415ba5000c5dbaf15b56d91d137a60ee257b5ef31c17cb2fd54ed015220b1f8c'
            '21d85a4f6ea7856074a4eb5c5fce6a10e764d11ff4336e92c4f009815efebb0c')

prepare() {
  ttfdir="${srcdir}"/dejavu-fonts-ttf-${pkgver}/ttf
  for ttf in "${ttfdir}"/*.ttf; do
    fontforge -script cleaner.py $ttf
  done

  cd dejavu-fonts-ttf-$pkgver
  patch -Np1 -i ../remove-generic-name-assignment-and-aliasing.patch
}

package() {
  install -dm755 "${pkgdir}"/etc/fonts/conf.avail
  install -dm755 "${pkgdir}"/etc/fonts/conf.d
  install -dm755 "${pkgdir}"/usr/share/fonts/TTF

  cd "${srcdir}"/dejavu-fonts-ttf-${pkgver}
  install -m644 ttf/*.ttf "${pkgdir}"/usr/share/fonts/TTF/
  install -m644 fontconfig/*.conf "${pkgdir}"/etc/fonts/conf.avail/

  pushd "${pkgdir}"/etc/fonts/conf.avail
  for config in *; do
    ln -sf ../conf.avail/${config} ../conf.d/${config}
  done
  popd

  install -Dm644 LICENSE "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE
}
